package m.domenico.pokemon.integration.service;

import lombok.extern.slf4j.Slf4j;
import m.domenico.pokemon.dto.Pokemon;
import m.domenico.pokemon.integration.client.PokeApiClient;
import m.domenico.pokemon.integration.client.ShakespeareApiClient;
import m.domenico.pokemon.integration.error.UnexpectedClientError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class PokeDescriptionService {


    private final PokeApiClient pokeApiClient;
    private final ShakespeareApiClient shakespeareApiClient;

    @Autowired
    public PokeDescriptionService(PokeApiClient pokeApiClient, ShakespeareApiClient shakespeareApiClient) {
        this.pokeApiClient = pokeApiClient;
        this.shakespeareApiClient = shakespeareApiClient;
    }

    /**
     * Retrieve Pokemon with shakespeare description
     *
     * @param pokemonName Input pokemon name
     * @return Pokemin object with name and shakespeare description
     * @throws UnexpectedClientError external error due to http calls
     */
    public Pokemon getShakespeareDescription(String pokemonName) throws UnexpectedClientError {
        final Pokemon pokemon = pokeApiClient.findPokemon(pokemonName);
        pokemon.setDescription(shakespeareApiClient.retrieveShakespeareDescription(pokemon.getDescription()));
        return pokemon;
    }
}

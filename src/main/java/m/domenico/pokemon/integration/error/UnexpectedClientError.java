package m.domenico.pokemon.integration.error;

public class UnexpectedClientError extends BaseError {

    private static final long serialVersionUID = -5945795065491536127L;

    private final static int code = 503;

    public UnexpectedClientError(String message) {
        super(code, message);
    }

    public UnexpectedClientError(int code, String message) {
        super(code, message);
    }

    public UnexpectedClientError(String message, Throwable cause) {
        super(code, message, cause);
    }

    public UnexpectedClientError(int code, String message, Throwable cause) {
        super(code, message, cause);
    }
}

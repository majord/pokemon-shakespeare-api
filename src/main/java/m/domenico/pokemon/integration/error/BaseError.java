package m.domenico.pokemon.integration.error;

import lombok.Getter;

public class BaseError extends Throwable {
    private static final long serialVersionUID = -4624903789429177670L;

    @Getter
    private int code = 500; // response code for api

    public BaseError(String message) {
        super(message);
    }

    public BaseError(int code, String message) {
        super(message);
        this.code = code;
    }

    public BaseError(String message, Throwable cause) {
        super(message, cause);
    }

    public BaseError(int code, String message, Throwable cause) {
        super(message, cause);
        this.code = code;
    }
}

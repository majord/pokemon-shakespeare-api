package m.domenico.pokemon.integration.client;


import kong.unirest.HttpResponse;
import kong.unirest.HttpStatus;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import kong.unirest.json.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import m.domenico.pokemon.integration.error.UnexpectedClientError;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;

@Controller
@Slf4j
public class ShakespeareApiClient {

    @Value("${shakespeareapi.url}")
    private String url;


    /**
     * Translate text as shakespeare text
     *
     * @param value text to translate
     * @return translated text
     * @throws UnexpectedClientError error in http call
     *                               case 204: translation missing
     */
    public String retrieveShakespeareDescription(String value) throws UnexpectedClientError {
        log.debug(" Performing POST call to: {} with text body: {}", url, value);
        HttpResponse<JsonNode> response = Unirest.post(url)
                .header("Content-Type", "application/json")
                .body(new ShakespeareBody(value.trim().toLowerCase())).asJson();
        if (response.isSuccess()) {
            log.debug("Call Successful with response: {}", response.getBody().toPrettyString());
            final JSONObject contents = response.getBody().getObject().optJSONObject("contents");
            if (contents != null) {
                final String translated = contents.optString("translated");
                if (translated != null) {
                    return translated.trim();
                } else {
                    final String message = "Translation doesn't found from Shakespeare Api for input: ".concat(value);
                    log.error(message);
                    throw new UnexpectedClientError(HttpStatus.NO_CONTENT, message);
                }
            } else {
                final String message = "Translation body missing from Shakespeare Api for input: ".concat(value);
                log.error(message);
                throw new UnexpectedClientError(HttpStatus.NO_CONTENT, message);
            }
        } else {
            log.error(" Unexpected response from shakespeare api. Status Code: {} ,Status text: {}", response.getStatus(), response.getStatusText());
            throw new UnexpectedClientError(response.getStatus(), "Response from shakespeare api: " + response.getStatusText());
        }
    }

    @AllArgsConstructor
    @NoArgsConstructor
    private static class ShakespeareBody {

        @Getter
        @Setter
        private String text;
    }
}

package m.domenico.pokemon.integration.client;


import kong.unirest.HttpResponse;
import kong.unirest.HttpStatus;
import kong.unirest.JsonNode;
import kong.unirest.Unirest;
import kong.unirest.json.JSONArray;
import kong.unirest.json.JSONObject;
import lombok.extern.slf4j.Slf4j;
import m.domenico.pokemon.dto.Pokemon;
import m.domenico.pokemon.integration.error.UnexpectedClientError;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;

@Controller
@Slf4j
public class PokeApiClient {

    @Value("${pokeapi.url}")
    private String url;


    /**
     * Retrieve pokemon description from Pokemon name or number
     *
     * @param pokemonName Name of pokemon or pokemon id
     * @return Pokemon class with name and description
     * @throws UnexpectedClientError Error with external call:
     *                               - Code: 404: Case pokemon not found
     *                               - Code: 204: Case some information missing
     */
    public Pokemon findPokemon(String pokemonName) throws UnexpectedClientError {
        log.debug("Performing GET call to {}{} ", url, pokemonName);
        HttpResponse<JsonNode> response = Unirest.get(url.concat(pokemonName.trim().toLowerCase())).asJson();
        if (response.isSuccess()) {
            final String NAME_TOKEN = "name"; // allocate memory only if success
            final String name = response.getBody().getObject().optString(NAME_TOKEN);
            if (name == null) {
                final String message = "Name from PokeApi Missing for input: ".concat(pokemonName);
                log.error(message);
                throw new UnexpectedClientError(HttpStatus.NO_CONTENT, message);
            }
            log.debug(" Name found with value: {} fon input: {} ", name, pokemonName);
            final String DESCRIPTION_TOKEN = "flavor_text_entries";
            final JSONArray textFlavors = response.getBody().getObject().optJSONArray(DESCRIPTION_TOKEN);
            String description = null;
            if (textFlavors != null) {
                // Iterate on array to search desired language "en" and version "red"since i played it when i was young
                final String LANGUAGE_TOKEN = "language";
                final String VERSION_TOKEN = "version";
                final String DESIRED_LANG_VALUE = "en";
                final String DESIRED_VERSION = "red";
                for (int i = 0; i < textFlavors.length(); i++) {
                    final JSONObject flavor = textFlavors.getJSONObject(i);
                    final JSONObject language = flavor.optJSONObject(LANGUAGE_TOKEN);
                    final JSONObject version = flavor.optJSONObject(VERSION_TOKEN);
                    if (language != null && version != null) {
                        final String lang = language.optString(NAME_TOKEN);
                        final String ver = version.optString(NAME_TOKEN);
                        if (lang != null && ver != null && lang.equalsIgnoreCase(DESIRED_LANG_VALUE) && ver.equalsIgnoreCase(DESIRED_VERSION)) {
                            description = flavor.optString("flavor_text");
                            // found it
                            break;
                        }
                    }
                }
                if (description == null || description.isEmpty()) {
                    final String message = "Pokemon Description not found for language: ".concat(DESIRED_LANG_VALUE)
                            .concat("from PokeApi for input: ")
                            .concat(pokemonName);
                    log.error(message);
                    throw new UnexpectedClientError(HttpStatus.NO_CONTENT, message);
                }
                log.debug(" Description found with value: {} fon input: {} ", description, pokemonName);
            } else {
                final String message = "Pokemon Description from PokeApi Missing for input: ".concat(pokemonName);
                log.error(message);
                throw new UnexpectedClientError(HttpStatus.NO_CONTENT, message);
            }

            return new Pokemon(name, description);
        } else {
            log.info("PokeApi call failed with code: {} and status text: {} ", response.getStatus(), response.getStatusText());
            String message;
            // Actually only error i could test
            if (response.getStatus() == HttpStatus.NOT_FOUND) {
                message = "Pokemon Not Found";
            } else {
                message = "PokeApi Response: " + response.getStatusText();
            }
            throw new UnexpectedClientError(response.getStatus(), message);
        }
    }
}

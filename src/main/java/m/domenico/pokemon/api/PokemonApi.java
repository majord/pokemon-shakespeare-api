package m.domenico.pokemon.api;


import io.swagger.v3.oas.annotations.ExternalDocumentation;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import m.domenico.pokemon.dto.Pokemon;
import m.domenico.pokemon.integration.error.UnexpectedClientError;
import m.domenico.pokemon.integration.service.PokeDescriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/pokemon")
@Slf4j
@OpenAPIDefinition(info = @Info(
        title = "Pokemon Shakespeare Description",
        description = "Retrieve from 'https://funtranslations.com/api/shakespeare' the pokemon description. Pokemon data is fetch from 'https://pokeapi.co/'",
        version = "1.0.0",
        contact = @Contact(name = "Montesanto Domenico", email = "m.domeniko@gmail.com")),
        externalDocs = @ExternalDocumentation(description = "Pokemon Api", url = "https://pokeapi.co/docs/v2"))
public class PokemonApi {

    final
    PokeDescriptionService service;

    @Autowired
    public PokemonApi(PokeDescriptionService service) {
        this.service = service;
    }


    @Operation(summary = "Retrieve shakespeare description of pokemon")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Pokemon and Description found",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = Pokemon.class))}),
            @ApiResponse(responseCode = "404", description = "Pokemon Not Found",
                    content = @Content),
            @ApiResponse(responseCode = "400", description = "Pokemon name is empty or invalid",
                    content = @Content),
            @ApiResponse(responseCode = "204", description = "Name or Description not present",
                    content = @Content)})
    @GetMapping(value = "{name}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Pokemon> pokeDescription(@Parameter(name = "Pokemon Name", example = "ditto") @PathVariable("name") String name) {
        log.info(" New call for Pokemon Shakespeare description with input: {}", name);
        if (name.trim().isEmpty()) {
            log.error(" Input pokemon name cannot be empty");
            return ResponseEntity.badRequest().build();
        }
        try {
            return ResponseEntity.ok(service.getShakespeareDescription(name));
        } catch (UnexpectedClientError error) {
            return ResponseEntity.status(error.getCode()).build();
        }
    }

}

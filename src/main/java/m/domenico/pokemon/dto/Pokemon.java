package m.domenico.pokemon.dto;


import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Pokemon implements Serializable {

    private static final long serialVersionUID = 5813133643582297328L;
    @Schema(name = "name", example = "ditto")
    private String name;

    @Schema(name = "description", example = "'t can freely recombine its own cellular structure to transform into other life-forms.")
    private String description;
}

package m.domenico.pokemon.integration.client;

import io.specto.hoverfly.junit.core.Hoverfly;
import io.specto.hoverfly.junit5.HoverflyExtension;
import kong.unirest.Unirest;
import m.domenico.pokemon.integration.TestData;
import m.domenico.pokemon.integration.error.UnexpectedClientError;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static io.specto.hoverfly.junit.core.SimulationSource.dsl;
import static io.specto.hoverfly.junit.dsl.HoverflyDsl.service;
import static io.specto.hoverfly.junit.dsl.ResponseCreators.success;

@SpringBootTest
@ExtendWith(HoverflyExtension.class)
class ShakespeareApiClientTest extends TestData {

    final String BASE_URL = "https://api.funtranslations.com";
    final String BASE_METHOD = "/translate/shakespeare";

    @Autowired
    ShakespeareApiClient client;


    @BeforeEach
    public void disableSSL() {
        Unirest.config().verifySsl(false);
    }

    @Test
    @DisplayName("Shakespeare successful call")
    public void successfulCall(Hoverfly hoverfly) throws UnexpectedClientError {
        hoverfly.simulate(dsl(
                service(BASE_URL)
                        .post(BASE_METHOD).anyBody()
                        .willReturn(success(SHAKESPEARE_200, "application/json"))
        ));
        String description = client.retrieveShakespeareDescription(TEST_FLOWER);
        Assertions.assertNotNull(description);
        Assertions.assertNotEquals(TEST_FLOWER, description);
        Assertions.assertEquals(SUCCESSFUL_TRANSLATION, description);
    }

}

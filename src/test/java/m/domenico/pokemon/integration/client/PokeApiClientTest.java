package m.domenico.pokemon.integration.client;

import io.specto.hoverfly.junit.core.Hoverfly;
import io.specto.hoverfly.junit.dsl.ResponseCreators;
import io.specto.hoverfly.junit5.HoverflyExtension;
import kong.unirest.Unirest;
import m.domenico.pokemon.dto.Pokemon;
import m.domenico.pokemon.integration.TestData;
import m.domenico.pokemon.integration.error.UnexpectedClientError;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static io.specto.hoverfly.junit.core.SimulationSource.dsl;
import static io.specto.hoverfly.junit.dsl.HoverflyDsl.service;
import static io.specto.hoverfly.junit.dsl.ResponseCreators.success;


@SpringBootTest
@ExtendWith(HoverflyExtension.class)
class PokeApiClientTest extends TestData {

    final String BASE_URL = "https://pokeapi.co";
    final String BASE_METHOD = "/api/v2/pokemon-species/";
    @Autowired
    private PokeApiClient client;

    @BeforeEach
    public void disableSSL() {
        Unirest.config().verifySsl(false);
    }


    @Test
    @DisplayName("PokeApi successful call")
    public void pokeApiCallSuccessful(Hoverfly hoverfly) throws UnexpectedClientError {
        hoverfly.simulate(dsl(
                service(BASE_URL)
                        .get(BASE_METHOD + TEST_REAL_POKEMON)
                        .willReturn(success(DITTO_JSON, "application/json"))
        ));
        Pokemon pokemon = client.findPokemon(TEST_REAL_POKEMON);
        Assertions.assertNotNull(pokemon);
        Assertions.assertEquals(TEST_REAL_POKEMON, pokemon.getName());
        Assertions.assertNotNull(pokemon.getDescription());
    }

    @Test
    @DisplayName("PokeApi invalid pokemon call")
    public void pokeApiCallInvalidPokemon(Hoverfly hoverfly) {
        hoverfly.simulate(dsl(
                service(BASE_URL)
                        .get(BASE_METHOD + TEST_INVALID_POKEMON)
                        .willReturn(ResponseCreators.notFound()))
        );
        Assertions.assertThrows(UnexpectedClientError.class, () -> {
            client.findPokemon(TEST_INVALID_POKEMON);
        });
    }

    @Test
    @DisplayName("PokeApi invalid pokemon call")
    public void test(Hoverfly hoverfly) {
        hoverfly.simulate(dsl(
                service(BASE_URL)
                        .get(BASE_METHOD)
                        .willReturn(ResponseCreators.notFound()))
        );
        Assertions.assertThrows(UnexpectedClientError.class, () -> {
            client.findPokemon("");
        });
    }

}

package m.domenico.pokemon.integration.service;

import m.domenico.pokemon.dto.Pokemon;
import m.domenico.pokemon.integration.TestData;
import m.domenico.pokemon.integration.client.PokeApiClient;
import m.domenico.pokemon.integration.client.ShakespeareApiClient;
import m.domenico.pokemon.integration.error.UnexpectedClientError;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@SpringBootTest
class PokeDescriptionServiceTest extends TestData {
    @InjectMocks
    PokeDescriptionService service;
    @Mock
    private PokeApiClient poleApiClient;
    @Mock
    private ShakespeareApiClient shakespeareClient;

    @BeforeEach
    public void mock() throws UnexpectedClientError {
        when(poleApiClient.findPokemon(eq(TEST_REAL_POKEMON))).thenReturn(new Pokemon(TEST_REAL_POKEMON, TEST_FLOWER));
        when(poleApiClient.findPokemon(eq(TEST_INVALID_POKEMON))).thenThrow(new UnexpectedClientError(404, "Not Found"));
        when(shakespeareClient.retrieveShakespeareDescription(eq(TEST_FLOWER))).thenReturn(SUCCESSFUL_TRANSLATION);
    }

    @Test
    @DisplayName("Real Pokemon")
    void getShakespeareDescription() throws UnexpectedClientError {
        final Pokemon poke = service.getShakespeareDescription(TEST_REAL_POKEMON);
        Assertions.assertNotNull(poke);
        Assertions.assertEquals(SUCCESSFUL_TRANSLATION, poke.getDescription());
        Assertions.assertNotEquals(TEST_FLOWER, poke.getDescription());
    }

    @Test
    @DisplayName(" Pokemon not found ")
    void pokemonNotFound() {
        Assertions.assertThrows(UnexpectedClientError.class, () -> service.getShakespeareDescription(TEST_INVALID_POKEMON));
    }
}

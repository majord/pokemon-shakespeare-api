package m.domenico.pokemon.integration;

import m.domenico.pokemon.dto.Pokemon;

public abstract class TestData {

    protected static final String TEST_REAL_POKEMON = "ditto";
    protected static final String TEST_INVALID_POKEMON = "aaaaaaaaa";
    protected static final String TEST_FLOWER = "It can freely recombine its own cellular structure to\\ntransform into other life-forms.";
    protected static final String SUCCESSFUL_TRANSLATION = "'t can freely recombine its own cellular structure to\\ntransform into other life-forms.";
    protected static final Pokemon SUCCESS_POKEMON = new Pokemon(TEST_REAL_POKEMON, SUCCESSFUL_TRANSLATION);
    protected static final String SUCCESSFUL_JSON_POKEMON = "{\"name\":\"ditto\",\"description\":\"'t can freely recombine its own cellular structure to\\\\ntransform into other life-forms.\"}";

    // real case
    protected final String SHAKESPEARE_200 = "{\n" +
            "  \"success\": {\n" +
            "    \"total\": 1\n" +
            "  },\n" +
            "  \"contents\": {\n" +
            "    \"translated\": \"\\u0027t can freely recombine its own cellular structure to\\\\ntransform into other life-forms.\",\n" +
            "    \"text\": \"it can freely recombine its own cellular structure to\\\\ntransform into other life-forms.\",\n" +
            "    \"translation\": \"shakespeare\"\n" +
            "  }\n" +
            "}";
    // This json is real case
    protected final String DITTO_JSON =
            "{\n" +
                    "  \"base_happiness\": 70,\n" +
                    "  \"capture_rate\": 35,\n" +
                    "  \"color\": {\n" +
                    "    \"name\": \"purple\",\n" +
                    "    \"url\": \"https://pokeapi.co/api/v2/pokemon-color/7/\"\n" +
                    "  },\n" +
                    "  \"egg_groups\": [\n" +
                    "    {\n" +
                    "      \"name\": \"ditto\",\n" +
                    "      \"url\": \"https://pokeapi.co/api/v2/egg-group/13/\"\n" +
                    "    }\n" +
                    "  ],\n" +
                    "  \"evolution_chain\": {\n" +
                    "    \"url\": \"https://pokeapi.co/api/v2/evolution-chain/66/\"\n" +
                    "  },\n" +
                    "  \"evolves_from_species\": null,\n" +
                    "  \"flavor_text_entries\": [\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"It can freely recombine its own cellular structure to\\ntransform into other life-forms.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"en\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/9/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"y\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/24/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"??????????\\n?????????\\n??????????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ja\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/11/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"y\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/24/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Capable of copying\\nan enemy\\u0027s genetic\\ncode to instantly\\ftransform itself\\ninto a duplicate\\nof the enemy.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"en\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/9/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"red\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/1/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Capable of copying\\nan enemy\\u0027s genetic\\ncode to instantly\\ftransform itself\\ninto a duplicate\\nof the enemy.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"en\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/9/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"blue\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/2/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"When it spots an\\nenemy, its body\\ntransfigures into\\fan almost perfect\\ncopy of its oppo�\\nnent.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"en\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/9/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"yellow\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/3/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"It can transform\\ninto anything.\\nWhen it sleeps, it\\fchanges into a\\nstone to avoid\\nbeing attacked.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"en\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/9/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"gold\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/4/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Its transformation\\nability is per�\\nfect. However, if\\fmade to laugh, it\\ncan\\u0027t maintain its\\ndisguise.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"en\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/9/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"silver\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/5/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"When it encount�\\ners another DITTO,\\nit will move\\ffaster than normal\\nto duplicate that\\nopponent exactly.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"en\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/9/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"crystal\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/6/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"DITTO rearranges its cell structure to\\ntransform itself into other shapes.\\nHowever, if it tries to transform itself\\finto something by relying on its memory,\\nthis POK�MON manages to get details\\nwrong.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"en\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/9/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"ruby\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/7/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"DITTO rearranges its cell structure to\\ntransform itself into other shapes.\\nHowever, if it tries to transform itself\\finto something by relying on its memory,\\nthis POK�MON manages to get details\\nwrong.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"en\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/9/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"sapphire\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/8/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"A DITTO rearranges its cell structure to\\ntransform itself. However, if it tries to\\nchange based on its memory, it will get\\ndetails wrong.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"en\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/9/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"emerald\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/9/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"It can freely recombine its own cellular\\nstructure to transform into other life-\\nforms.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"en\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/9/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"firered\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/10/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Capable of copying an opponent�s genetic\\ncode to instantly transform itself into a\\nduplicate of the enemy.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"en\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/9/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"leafgreen\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/11/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"It has the ability to reconstitute\\nits entire cellular structure to\\ntransform into whatever it sees.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"en\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/9/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"diamond\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/12/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"It has the ability to reconstitute\\nits entire cellular structure to\\ntransform into whatever it sees.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"en\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/9/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"pearl\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/13/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"It has the ability to reconstitute\\nits entire cellular structure to\\ntransform into whatever it sees.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"en\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/9/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"platinum\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/14/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"It can transform into anything.\\nWhen it sleeps, it changes into a\\nstone to avoid being attacked.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"en\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/9/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"heartgold\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/15/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Its transformation ability is perfect.\\nHowever, if made to laugh, it\\ncan�t maintain its disguise.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"en\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/9/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"soulsilver\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/16/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Il a la capacit� de modifier sa\\nstructure cellulaire pour prendre\\nl�apparence de ce qu�il voit.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"fr\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/5/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"black\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/17/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"It has the ability to reconstitute\\nits entire cellular structure to\\ntransform into whatever it sees.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"en\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/9/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"black\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/17/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Il a la capacit� de modifier sa\\nstructure cellulaire pour prendre\\nl�apparence de ce qu�il voit.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"fr\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/5/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"white\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/18/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"It has the ability to reconstitute\\nits entire cellular structure to\\ntransform into whatever it sees.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"en\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/9/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"white\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/18/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"It can reconstitute its entire cellular\\nstructure to change into what it sees,\\nbut it returns to normal when it relaxes.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"en\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/9/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"black-2\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/21/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"It can reconstitute its entire cellular\\nstructure to change into what it sees,\\nbut it returns to normal when it relaxes.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"en\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/9/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"white-2\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/22/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"?????????????????\\n???????????????\\n?????????????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ja-Hrkt\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/1/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"x\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/23/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"??? ??? ?????\\n? ?? ??? ? ??\\n???? ??? ??? ??.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ko\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/3/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"x\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/23/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Il a la capacit� de modifier sa structure cellulaire pour\\nprendre l�apparence de ce qu�il voit.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"fr\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/5/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"x\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/23/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Es kann seine Zellstruktur so ver�ndern, dass es\\nsich in alles verwandeln kann, was es sieht.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"de\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/6/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"x\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/23/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Puede alterar por completo su estructura celular para\\nemular cualquier objeto que vea.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"es\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/7/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"x\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/23/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Ha la capacit� di modificare la sua struttura cellulare\\nper trasformarsi in qualsiasi cosa veda.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"it\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/8/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"x\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/23/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"It has the ability to reconstitute its entire cellular\\nstructure to transform into whatever it sees.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"en\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/9/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"x\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/23/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"?????????????\\n?????????????\\n????????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ja\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/11/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"x\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/23/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"???????????????\\n??????????????\\n???????????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ja-Hrkt\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/1/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"y\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/24/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"?? ?? ???\\n??? ??? ??\\n???? ????.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ko\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/3/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"y\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/24/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"M�tamorph peut modifier sa structure cellulaire �\\nsa guise pour se transformer en n�importe quelle\\nforme vivante.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"fr\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/5/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"y\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/24/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Es kann seine eigene Zellstruktur frei zusammensetzen\\nund sich in jede andere Lebensform verwandeln.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"de\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/6/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"y\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/24/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Tiene la capacidad de reorganizar su estructura celular\\npara convertirse en otras formas de vida.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"es\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/7/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"y\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/24/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Pu� ricombinare a piacere la propria struttura\\ncellulare per trasformarsi in altri esseri viventi.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"it\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/8/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"y\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/24/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"????????????????????????\\n????????????????????????\\n???????????????????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ja-Hrkt\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/1/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"omega-ruby\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/25/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"?? ??? ????? ????.\\n?? ?? ?? ?????? ???\\n?? ?? ??? ?????.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ko\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/3/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"omega-ruby\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/25/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"M�tamorph peut modifier sa structure mol�culaire pour\\nprendre d�autres formes. Lorsqu�il essaie de se transformer\\nde m�moire, il lui arrive de se tromper sur certains d�tails.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"fr\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/5/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"omega-ruby\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/25/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Ditto ver�ndert seine Zellstruktur, um sich in eine andere Form\\nzu verwandeln. Wenn es sich dabei jedoch auf sein Ged�chtnis\\nverl�sst, unterlaufen diesem Pok�mon schon mal Fehler.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"de\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/6/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"omega-ruby\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/25/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Ditto reorganiza la estructura de sus c�lulas para adoptar\\notras formas. Pero, como intente transformarse en algo\\ngui�ndose por los datos que tenga almacenados en la\\nmemoria, habr� detalles que se le escapen.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"es\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/7/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"omega-ruby\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/25/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Ditto cambia la sua struttura cellulare per assumere molte\\naltre forme. Tuttavia, quando si affida solo alla sua memoria,\\ntalvolta dimentica dettagli importanti.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"it\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/8/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"omega-ruby\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/25/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Ditto rearranges its cell structure to transform itself into other\\nshapes. However, if it tries to transform itself into something\\nby relying on its memory, this Pok�mon manages to get\\ndetails wrong.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"en\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/9/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"omega-ruby\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/25/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"??????????????????\\n??????????????????????\\n????????????????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ja\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/11/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"omega-ruby\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/25/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"????????????????????????\\n????????????????????????\\n???????????????????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ja-Hrkt\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/1/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"alpha-sapphire\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/26/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"?? ??? ????? ????.\\n?? ?? ?? ?????? ???\\n?? ?? ??? ?????.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ko\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/3/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"alpha-sapphire\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/26/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"M�tamorph peut modifier sa structure mol�culaire pour\\nprendre d�autres formes. Lorsqu�il essaie de se transformer\\nde m�moire, il lui arrive de se tromper sur certains d�tails.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"fr\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/5/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"alpha-sapphire\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/26/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Ditto ver�ndert seine Zellstruktur, um sich in eine andere Form\\nzu verwandeln. Wenn es sich dabei jedoch auf sein Ged�chtnis\\nverl�sst, unterlaufen diesem Pok�mon schon mal Fehler.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"de\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/6/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"alpha-sapphire\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/26/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Ditto reorganiza la estructura de sus c�lulas para adoptar\\notras formas. Pero, como intente transformarse en algo\\ngui�ndose por los datos que tenga almacenados en la\\nmemoria, habr� detalles que se le escapen.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"es\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/7/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"alpha-sapphire\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/26/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Ditto cambia la sua struttura cellulare per assumere molte\\naltre forme. Tuttavia, quando si affida solo alla sua memoria,\\ntalvolta dimentica dettagli importanti.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"it\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/8/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"alpha-sapphire\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/26/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Ditto rearranges its cell structure to transform itself into other\\nshapes. However, if it tries to transform itself into something\\nby relying on its memory, this Pok�mon manages to get\\ndetails wrong.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"en\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/9/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"alpha-sapphire\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/26/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"??????????????????\\n??????????????????????\\n????????????????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ja\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/11/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"alpha-sapphire\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/26/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"?????????????????\\n??????????????????\\n???????????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ja-Hrkt\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/1/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"sun\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/27/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"??? ????? ? ???\\n??? ??? ?????.\\n???? ??? ?? ???.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ko\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/3/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"sun\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/27/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"??????????????\\n???????????\\n??????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"zh-Hant\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/4/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"sun\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/27/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Il peut modifier sa structure mol�culaire pour\\nprendre l�apparence de son adversaire. Le degr�\\nde ressemblance d�pend de chaque individu.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"fr\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/5/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"sun\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/27/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Es ver�ndert seine Zellstruktur, um die Gestalt\\nanderer Lebewesen, die es sieht, anzunehmen.\\nWie gut das Ebenbild ist, variiert nach Exemplar.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"de\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/6/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"sun\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/27/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Ditto reorganiza la estructura de sus c�lulas\\npara adoptar la forma de su oponente. La\\ncalidad de la copia var�a de Ditto a Ditto.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"es\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/7/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"sun\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/27/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Cambia la sua struttura molecolare per\\nassumere le sembianze del nemico. Il grado di\\nriuscita varia a seconda del Pok�mon.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"it\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/8/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"sun\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/27/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"It can reorganize its cells to make itself into a\\nduplicate of anything it sees. The quality of the\\nduplicate depends on the individual.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"en\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/9/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"sun\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/27/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"??????????????\\n???????????????\\n????????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ja\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/11/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"sun\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/27/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"?????????????\\n???????????\\n????????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"zh-Hans\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/12/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"sun\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/27/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"????????????????\\n?????????????????\\n?????????????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ja-Hrkt\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/1/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"moon\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/28/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"??? ?? ????\\n???? ??? ? ? ??.\\n?????? ??? ???.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ko\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/3/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"moon\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/28/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"??????????\\n???????????\\n???????????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"zh-Hant\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/4/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"moon\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/28/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Gr�ce � son don de transformation, ce Pok�mon\\npeut devenir l�ami de tous les �tres vivants, sauf\\ndes autres M�tamorph, qu�il d�teste.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"fr\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/5/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"moon\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/28/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Dank seiner F�higkeiten als Gestaltwandler\\nkann es sich mit allen Lebewesen anfreunden.\\nNur mit Artgenossen kommt es nicht zurecht.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"de\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/6/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"moon\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/28/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Ditto puede hacerse amigo de otros Pok�mon\\ngracias a su habilidad para transformarse. Sin\\nembargo, los Ditto no se llevan bien entre s�.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"es\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/7/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"moon\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/28/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Grazie alle sue incredibili doti metamorfiche,\\npu� assumere le sembianze di qualunque essere\\nvivente. I Ditto non vanno d�accordo fra di loro.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"it\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/8/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"moon\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/28/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"With its astonishing capacity for\\nmetamorphosis, it can get along with anything.\\nIt does not get along well with its fellow Ditto.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"en\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/9/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"moon\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/28/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"?????????\\n???????????????\\n??????????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ja\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/11/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"moon\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/28/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"??????????\\n???????????\\n?????????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"zh-Hans\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/12/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"moon\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/28/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"?????????????????\\n????????????????\\n???????????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ja-Hrkt\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/1/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"ultra-sun\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/29/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"??? ??? ? ???\\n????? ??? ???\\n? ??? ??? ??? ??.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ko\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/3/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"ultra-sun\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/29/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"??????????\\n???????????\\n????????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"zh-Hant\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/4/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"ultra-sun\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/29/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Il peut prendre l�apparence de n�importe quoi,\\nmais chaque M�tamorph a son propre domaine\\nde pr�dilection et ses propres faiblesses.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"fr\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/5/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"ultra-sun\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/29/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Ditto kann jede beliebige Gestalt annehmen,\\nwobei aber jedes Exemplar individuelle\\nSt�rken und Schw�chen aufweist.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"de\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/6/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"ultra-sun\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/29/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Los Ditto pueden adoptar cualquier aspecto,\\npero cada individuo tiene un objeto de imitaci�n\\nque se le da mejor que otros.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"es\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/7/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"ultra-sun\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/29/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Ditto � in grado di assumere le sembianze di\\nqualsiasi cosa. Ogni esemplare riesce meglio\\nin alcune trasformazioni piuttosto che in altre.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"it\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/8/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"ultra-sun\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/29/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"While it can transform into anything, each Ditto\\napparently has its own strengths and\\nweaknesses when it comes to transformations.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"en\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/9/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"ultra-sun\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/29/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"???????????????\\n???????????????\\n??????????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ja\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/11/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"ultra-sun\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/29/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"??????????\\n?????????????\\n????????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"zh-Hans\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/12/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"ultra-sun\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/29/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"??????????????????\\n????????????????\\n??????????????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ja-Hrkt\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/1/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"ultra-moon\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/30/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"? ?? ?? ??? ????.\\n? ??? ?? ??? ??? ??\\n??? ??? ??? ????? ??.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ko\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/3/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"ultra-moon\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/30/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"????????????\\n??????????????\\n???????????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"zh-Hant\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/4/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"ultra-moon\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/30/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Il prend la forme de ce qu�il voit. Lorsqu�il n�a\\npas de mod�le sous les yeux, il doit se fier � sa\\nm�moire et le r�sultat laisse parfois � d�sirer.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"fr\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/5/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"ultra-moon\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/30/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Es kann sich in alles verwandeln, was es sieht.\\nMuss es sich dabei aber auf sein Ged�chtnis\\nverlassen, macht es manchmal auch Fehler.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"de\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/6/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"ultra-moon\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/30/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Puede transformarse en cualquier cosa que vea,\\npero, si intenta hacerlo de memoria, habr�\\ndetalles que se le escapen.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"es\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/7/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"ultra-moon\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/30/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Si trasforma in qualsiasi cosa veda. Tuttavia,\\nquando non ha di fronte il suo modello si affida\\nalla memoria, e talvolta sbaglia.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"it\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/8/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"ultra-moon\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/30/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"It transforms into whatever it sees. If the thing\\nit�s transforming into isn�t right in front of it,\\nDitto relies on its memory�so sometimes it fails.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"en\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/9/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"ultra-moon\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/30/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"????????????????\\n???????????????\\n??????????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ja\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/11/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"ultra-moon\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/30/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"???????????\\n???????????????\\n???????????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"zh-Hans\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/12/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"ultra-moon\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/30/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"?????????????????\\n?????????????????\\n??????????????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ja-Hrkt\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/1/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"lets-go-pikachu\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/31/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"??? ? ?? ??\\n?? ??? ???? ????.\\n?? ??? ???? ????.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ko\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/3/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"lets-go-pikachu\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/31/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"?????????????\\n?????????????\\n???????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"zh-Hant\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/4/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"lets-go-pikachu\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/31/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Quand il rep�re un ennemi, il adapte son corps\\npour en faire une copie presque parfaite de\\ncelui de son adversaire.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"fr\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/5/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"lets-go-pikachu\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/31/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Wenn es einen Gegner ausmacht, verwandelt es\\nseinen K�rper in eine nahezu perfekte Kopie\\nseines Gegen�bers.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"de\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/6/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"lets-go-pikachu\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/31/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Cuando se fija en un enemigo, su cuerpo se\\ntransforma en una copia casi perfecta del\\nmismo.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"es\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/7/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"lets-go-pikachu\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/31/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Quando incontra un nemico � capace di\\ntrasformarsi in un baleno in una sua copia\\nquasi perfetta.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"it\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/8/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"lets-go-pikachu\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/31/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"When it spots an enemy, its body transfigures\\ninto an almost-perfect copy of its opponent.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"en\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/9/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"lets-go-pikachu\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/31/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"???????????\\n???????????????\\n?????????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ja\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/11/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"lets-go-pikachu\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/31/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"?????????????\\n?????????????\\n??????????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"zh-Hans\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/12/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"lets-go-pikachu\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/31/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"?????????????????\\n?????????????????\\n??????????????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ja-Hrkt\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/1/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"lets-go-eevee\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/32/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"??? ? ?? ??\\n?? ??? ???? ????.\\n?? ??? ???? ????.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ko\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/3/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"lets-go-eevee\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/32/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"?????????????\\n?????????????\\n???????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"zh-Hant\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/4/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"lets-go-eevee\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/32/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Quand il rep�re un ennemi, il adapte son corps\\npour en faire une copie presque parfaite de\\ncelui de son adversaire.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"fr\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/5/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"lets-go-eevee\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/32/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Wenn es einen Gegner ausmacht, verwandelt es\\nseinen K�rper in eine nahezu perfekte Kopie\\nseines Gegen�bers.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"de\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/6/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"lets-go-eevee\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/32/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Cuando se fija en un enemigo, su cuerpo se\\ntransforma en una copia casi perfecta del\\nmismo.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"es\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/7/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"lets-go-eevee\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/32/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Quando incontra un nemico � capace di\\ntrasformarsi in un baleno in una sua copia\\nquasi perfetta.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"it\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/8/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"lets-go-eevee\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/32/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"When it spots an enemy, its body transfigures\\ninto an almost-perfect copy of its opponent.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"en\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/9/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"lets-go-eevee\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/32/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"???????????\\n???????????????\\n?????????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ja\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/11/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"lets-go-eevee\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/32/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"?????????????\\n?????????????\\n??????????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"zh-Hans\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/12/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"lets-go-eevee\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/32/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"?????????????????\\n??????????????????\\n?????????????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ja-Hrkt\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/1/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"sword\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/33/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"??? ??? ?????\\n? ?? ? ?? ?????\\n?? ??? ???? ????.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ko\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/3/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"sword\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/33/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"????????????\\n?????????????\\n?????????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"zh-Hant\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/4/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"sword\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/33/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Il modifie sa structure cellulaire pour copier\\nl�apparence de ce qu�il voit, mais au repos,\\nil reprend sa forme normale.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"fr\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/5/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"sword\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/33/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Es modifiziert seine Zellstruktur, um sich in alles\\nzu verwandeln, was es sieht. Im entspannten\\nZustand nimmt es wieder seine Ausgangsform an.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"de\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/6/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"sword\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/33/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Redistribuye las c�lulas de su cuerpo para cobrar\\nla apariencia de lo que ve, pero vuelve a la\\nnormalidad al relajarse.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"es\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/7/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"sword\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/33/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Pu� modificare la sua struttura cellulare per\\nassumere le sembianze di ci� che vede.\\nRitorna se stesso quando � stanco.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"it\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/8/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"sword\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/33/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"It can reconstitute its entire cellular\\nstructure to change into what it sees,\\nbut it returns to normal when it relaxes.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"en\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/9/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"sword\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/33/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"?????????????\\n????????????????\\n???????????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ja\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/11/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"sword\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/33/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"????????????\\n?????????????\\n?????????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"zh-Hans\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/12/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"sword\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/33/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"????????????????\\n??????????????????\\n???????????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ja-Hrkt\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/1/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"shield\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/34/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"????? ??? ??\\n??? ??? ??? ?? ??\\n???? ???? ????.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ko\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/3/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"shield\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/34/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"????????????\\n??????????????\\n??????????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"zh-Hant\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/4/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"shield\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/34/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Quand il rencontre l�un de ses semblables,\\nil s�agite avec plus de vivacit� que d�habitude\\npour adopter exactement la m�me forme que lui.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"fr\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/5/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"shield\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/34/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Trifft es auf ein anderes Ditto, bewegt es sich\\nschneller als gew�hnlich, um es exakt\\nnachzuahmen.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"de\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/6/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"shield\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/34/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Cuando se encuentra con otro Ditto, se mueve\\nm�s r�pido de lo normal para intentar adoptar\\nsu aspecto.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"es\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/7/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"shield\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/34/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"Quando incontra un altro Ditto, si muove pi�\\nvelocemente del solito nel tentativo di assumere\\nle sue sembianze.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"it\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/8/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"shield\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/34/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"When it encounters another Ditto, it will move\\nfaster than normal to duplicate that opponent exactly.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"en\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/9/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"shield\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/34/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"???????????????\\n???????????????\\n?????????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ja\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/11/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"shield\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/34/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"flavor_text\": \"????????????\\n??????????????\\n??????????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"zh-Hans\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/12/\"\n" +
                    "      },\n" +
                    "      \"version\": {\n" +
                    "        \"name\": \"shield\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/version/34/\"\n" +
                    "      }\n" +
                    "    }\n" +
                    "  ],\n" +
                    "  \"form_descriptions\": [],\n" +
                    "  \"forms_switchable\": false,\n" +
                    "  \"gender_rate\": -1,\n" +
                    "  \"genera\": [\n" +
                    "    {\n" +
                    "      \"genus\": \"????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ja-Hrkt\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/1/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"genus\": \"?????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ko\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/3/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"genus\": \"?????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"zh-Hant\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/4/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"genus\": \"Pok�mon Morphing\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"fr\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/5/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"genus\": \"Transform\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"de\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/6/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"genus\": \"Pok�mon Transform.\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"es\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/7/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"genus\": \"Pok�mon Mutante\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"it\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/8/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"genus\": \"Transform Pok�mon\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"en\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/9/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"genus\": \"????????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ja\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/11/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"genus\": \"?????\",\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"zh-Hans\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/12/\"\n" +
                    "      }\n" +
                    "    }\n" +
                    "  ],\n" +
                    "  \"generation\": {\n" +
                    "    \"name\": \"generation-i\",\n" +
                    "    \"url\": \"https://pokeapi.co/api/v2/generation/1/\"\n" +
                    "  },\n" +
                    "  \"growth_rate\": {\n" +
                    "    \"name\": \"medium\",\n" +
                    "    \"url\": \"https://pokeapi.co/api/v2/growth-rate/2/\"\n" +
                    "  },\n" +
                    "  \"habitat\": {\n" +
                    "    \"name\": \"urban\",\n" +
                    "    \"url\": \"https://pokeapi.co/api/v2/pokemon-habitat/8/\"\n" +
                    "  },\n" +
                    "  \"has_gender_differences\": false,\n" +
                    "  \"hatch_counter\": 20,\n" +
                    "  \"id\": 132,\n" +
                    "  \"is_baby\": false,\n" +
                    "  \"is_legendary\": false,\n" +
                    "  \"is_mythical\": false,\n" +
                    "  \"name\": \"ditto\",\n" +
                    "  \"names\": [\n" +
                    "    {\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ja-Hrkt\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/1/\"\n" +
                    "      },\n" +
                    "      \"name\": \"????\"\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"roomaji\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/2/\"\n" +
                    "      },\n" +
                    "      \"name\": \"Metamon\"\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ko\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/3/\"\n" +
                    "      },\n" +
                    "      \"name\": \"???\"\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"zh-Hant\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/4/\"\n" +
                    "      },\n" +
                    "      \"name\": \"???\"\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"fr\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/5/\"\n" +
                    "      },\n" +
                    "      \"name\": \"M�tamorph\"\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"de\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/6/\"\n" +
                    "      },\n" +
                    "      \"name\": \"Ditto\"\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"es\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/7/\"\n" +
                    "      },\n" +
                    "      \"name\": \"Ditto\"\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"it\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/8/\"\n" +
                    "      },\n" +
                    "      \"name\": \"Ditto\"\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"en\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/9/\"\n" +
                    "      },\n" +
                    "      \"name\": \"Ditto\"\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"ja\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/11/\"\n" +
                    "      },\n" +
                    "      \"name\": \"????\"\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"language\": {\n" +
                    "        \"name\": \"zh-Hans\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/language/12/\"\n" +
                    "      },\n" +
                    "      \"name\": \"???\"\n" +
                    "    }\n" +
                    "  ],\n" +
                    "  \"order\": 156,\n" +
                    "  \"pal_park_encounters\": [\n" +
                    "    {\n" +
                    "      \"area\": {\n" +
                    "        \"name\": \"field\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/pal-park-area/2/\"\n" +
                    "      },\n" +
                    "      \"base_score\": 70,\n" +
                    "      \"rate\": 20\n" +
                    "    }\n" +
                    "  ],\n" +
                    "  \"pokedex_numbers\": [\n" +
                    "    {\n" +
                    "      \"entry_number\": 132,\n" +
                    "      \"pokedex\": {\n" +
                    "        \"name\": \"national\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/pokedex/1/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"entry_number\": 132,\n" +
                    "      \"pokedex\": {\n" +
                    "        \"name\": \"kanto\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/pokedex/2/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"entry_number\": 92,\n" +
                    "      \"pokedex\": {\n" +
                    "        \"name\": \"original-johto\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/pokedex/3/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"entry_number\": 92,\n" +
                    "      \"pokedex\": {\n" +
                    "        \"name\": \"updated-johto\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/pokedex/7/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"entry_number\": 261,\n" +
                    "      \"pokedex\": {\n" +
                    "        \"name\": \"updated-unova\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/pokedex/9/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"entry_number\": 138,\n" +
                    "      \"pokedex\": {\n" +
                    "        \"name\": \"kalos-mountain\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/pokedex/14/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"entry_number\": 209,\n" +
                    "      \"pokedex\": {\n" +
                    "        \"name\": \"original-alola\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/pokedex/16/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"entry_number\": 81,\n" +
                    "      \"pokedex\": {\n" +
                    "        \"name\": \"original-ulaula\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/pokedex/19/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"entry_number\": 271,\n" +
                    "      \"pokedex\": {\n" +
                    "        \"name\": \"updated-alola\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/pokedex/21/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"entry_number\": 92,\n" +
                    "      \"pokedex\": {\n" +
                    "        \"name\": \"updated-ulaula\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/pokedex/24/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"entry_number\": 373,\n" +
                    "      \"pokedex\": {\n" +
                    "        \"name\": \"galar\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/pokedex/27/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"entry_number\": 207,\n" +
                    "      \"pokedex\": {\n" +
                    "        \"name\": \"isle-of-armor\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/pokedex/28/\"\n" +
                    "      }\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"entry_number\": 132,\n" +
                    "      \"pokedex\": {\n" +
                    "        \"name\": \"updated-kanto\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/pokedex/26/\"\n" +
                    "      }\n" +
                    "    }\n" +
                    "  ],\n" +
                    "  \"shape\": {\n" +
                    "    \"name\": \"ball\",\n" +
                    "    \"url\": \"https://pokeapi.co/api/v2/pokemon-shape/1/\"\n" +
                    "  },\n" +
                    "  \"varieties\": [\n" +
                    "    {\n" +
                    "      \"is_default\": true,\n" +
                    "      \"pokemon\": {\n" +
                    "        \"name\": \"ditto\",\n" +
                    "        \"url\": \"https://pokeapi.co/api/v2/pokemon/132/\"\n" +
                    "      }\n" +
                    "    }\n" +
                    "  ]\n" +
                    "}\n";


}

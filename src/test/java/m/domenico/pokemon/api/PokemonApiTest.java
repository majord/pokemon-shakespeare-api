package m.domenico.pokemon.api;

import m.domenico.pokemon.integration.TestData;
import m.domenico.pokemon.integration.error.UnexpectedClientError;
import m.domenico.pokemon.integration.service.PokeDescriptionService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class PokemonApiTest extends TestData {

    private final String POKE_METHOD = "/pokemon/";

    @Autowired
    private MockMvc api;

    @MockBean
    private PokeDescriptionService service;

    @BeforeEach
    public void mockData() throws UnexpectedClientError {
        when(service.getShakespeareDescription(eq(TEST_REAL_POKEMON))).thenReturn(SUCCESS_POKEMON);
        when(service.getShakespeareDescription(eq(TEST_INVALID_POKEMON))).thenThrow(new UnexpectedClientError(404, "Pokemon Not Found"));
    }

    @Test
    @DisplayName("Successful call")
    void pokeDescription() throws Exception {
        api.perform(get(POKE_METHOD + TEST_REAL_POKEMON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(content().json(SUCCESSFUL_JSON_POKEMON));
    }

    @Test
    @DisplayName("Bad Input call")
    void badInput() throws Exception {
        api.perform(get(POKE_METHOD+"  "))
                .andDo(print())
                .andExpect(status().isBadRequest());
    }

    @Test
    @DisplayName("Not Found call")
    void notfound() throws Exception {
        api.perform(get(POKE_METHOD + TEST_INVALID_POKEMON))
                .andDo(print())
                .andExpect(status().isNotFound());
    }
}

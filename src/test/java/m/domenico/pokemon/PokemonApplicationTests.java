package m.domenico.pokemon;

import m.domenico.pokemon.integration.service.PokeDescriptionService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class PokemonApplicationTests {

    @Autowired
    PokeDescriptionService service;

    @Test
    void contextLoads() {
        Assertions.assertNotNull(service);
    }

}

# Pokemon ShakeSpeare API

Pipeline:
[![pipeline status](https://gitlab.com/majord/pokemon-shakespeare-api/badges/master/pipeline.svg)](https://gitlab.com/majord/pokemon-shakespeare-api/-/commits/master)


## SCOPE

This application have purpose of retrieve a Shakespeare Description for the pokemon. From this API you can get every pokemon description present into pokemon (videogame) universe.

You can choose you preferred from here:  https://www.pokemon.com/us/pokedex/

For this purpose i choose to use description of RED version of the game ( the first one i ever played in my childhood )

## PRE REQUISITES

- Java 8+ (https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html)
- Maven 3.4+  (https://maven.apache.org/download.cgi)

Optional
- Docker (https://www.docker.com/products/docker-desktop)

## INSTRUCTIONS

For run this app we have two way: maven or docker. 

### Maven

For run as maven be sure to have maven installed and 8080 port free.

Just open a prompt command (ctrl+alt+t linux; Windows+R and type cmd for windows; Applications>Utilities>Terminal on macOS).

Move to download folder 

```shell script
cd <download_folder>/pokemon
```
Now run command for start server. 

````shell script
mvn spring-boot:run
````

For stop just close the prompt command or press Ctrl/Cmd+C

### Docker
For run as docker we need first to build image. 
Just open a prompt command (ctrl+alt+t linux; Windows+R and type cmd for windows; Applications>Utilities>Terminal on macOS).

Move to download folder 

```shell script
cd <download_folder>/pokemon
```
First of all build project: 

````shell script
mvn clean package
````
Let's build docker image:
```shell script
docker build  -t pokemon/description:1.0 .
```

be sure 8080 port is open and free
Ready to start!
```shell script
docker run -p 8080:8080 --name pokeApi pokemon/description:1.0
```

For stop just close the prompt command or press Ctrl/Cmd+C

## Use the API

When server is started you can go in your browser to this page: `http://localhost:8080/swagger-ui` click on `Try Out!` digit pokemon name and check the results.

Otherwise with HTTPie (https://httpie.io/)
```shell script
http http://localhost:8080/pokemon/{pokemonName}
```
